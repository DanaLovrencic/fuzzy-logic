#include "FuzzySystem.hpp"
#include "Assert.hpp"
#include "AccelerationRules.hpp"
#include "RudderRules.hpp"
#include "Config.hpp"

#include "Operation.hpp"
#include <iostream>

FuzzySystem::FuzzySystem(DefuzzifierP def) noexcept : def(std::move(def)) {}

int FuzzySystem::conclude(SystemInput input)
{
    ASSERT(rule_base.get_n_rules() > 0,
           "Rule base should have at least one rule.\n");

    auto sets = get_conslusions(input);
    auto fset = get_union(std::move(sets));

    return def->defuzzify(std::move(fset));
}

std::vector<IFuzzySetP>
FuzzySystem::get_conslusions(SystemInput input) const noexcept
{
    std::vector<IFuzzySetP> sets;

    for (const auto& rule : rule_base) {
        sets.push_back(rule->conclude(input));
    }

    return sets;
}

IFuzzySetP FuzzySystem::get_union(std::vector<IFuzzySetP> sets) const
{
    ASSERT(consequent, "Consequent is not initialized.\n");
    ASSERT(sets.size() > 0, "At least one conclusion should be given.\n");

    std::vector<double> max_values;
    for (std::size_t i = 0; i < consequent->get_domain().get_cardinality();
         ++i) {
        max_values.push_back(0.0);
    }

    for (std::size_t i = 0; i < max_values.size(); ++i) {
        for (const auto& s : sets) {
            const auto& d = s->get_domain();
            double value  = s->get_value_at(d.element_for_index(i));
            if (value > max_values[i]) { max_values[i] = value; }
        }
    }

    return std::make_unique<MutableFuzzySet>(get_consequent_domain(),
                                             max_values);
}

IDomainP FuzzySystem::get_consequent_domain() const
{
    const IDomain& d = consequent->get_domain();
    return Domain::int_range((*d.get_component(0).begin())[0],
                             (*d.get_component(0).begin())[0] +
                                 d.get_cardinality());
}

AccelerationFuzzySystem::AccelerationFuzzySystem(DefuzzifierP def,
                                                 IBinaryFunctionSP t_norm,
                                                 IBinaryFunctionSP impl)
    : FuzzySystem(std::move(def))
{
    rule_base.add_rule(std::make_unique<LAccelerationRule>(t_norm, impl));
    rule_base.add_rule(std::make_unique<RAccelerationRule>(t_norm, impl));
    rule_base.add_rule(std::make_unique<LKAccelerationRule>(t_norm, impl));
    rule_base.add_rule(std::make_unique<RKAccelerationRule>(t_norm, impl));
    rule_base.add_rule(std::make_unique<ClearAccelerationRule>(t_norm, impl));
    rule_base.add_rule(std::make_unique<DirectionRule>(t_norm, impl));
    rule_base.add_rule(std::make_unique<VelocityRule>(t_norm, impl));

    consequent = std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_acc, end_acc),
        StandardFuzzySets::l_function(-10, 10));
}

RudderFuzzySystem::RudderFuzzySystem(DefuzzifierP def,
                                     IBinaryFunctionSP t_norm,
                                     IBinaryFunctionSP impl)
    : FuzzySystem(std::move(def))
{
    rule_base.add_rule(std::make_unique<LRudderRule>(t_norm, impl));
    rule_base.add_rule(std::make_unique<RRudderRule>(t_norm, impl));
    rule_base.add_rule(std::make_unique<LKRudderRule>(t_norm, impl));
    rule_base.add_rule(std::make_unique<RKRudderRule>(t_norm, impl));

    consequent = std::make_unique<CalculatedFuzzySet>(
        Domain::int_range(start_ang, end_ang),
        StandardFuzzySets::l_function(-90, 90));
}

void FuzzySystem::print_conclusion(RuleP rule, SystemInput input)
{
    auto conclusion = rule->conclude(input);
    std::cerr << *conclusion << std::endl;
    std::cerr << def->defuzzify(std::move(conclusion)) << std::endl;
}

void FuzzySystem::print_conclusion(SystemInput input)
{
    auto sets = get_conslusions(input);
    auto fset = get_union(std::move(sets));

    std::cout << *fset << std::endl;
    std::cout << def->defuzzify(std::move(fset)) << std::endl;
}
