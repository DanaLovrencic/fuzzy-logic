#ifndef SYSTEM_INPUT_HPP
#define SYSTEM_INPUT_HPP

#include <string>
#include <vector>

class SystemInput {
  public:
    std::vector<int> input;
    int L, R, LK, RK, V, D;

  public:
    SystemInput(std::string input);
};

#endif
