#ifndef CONFIG_HPP
#define CONFIG_HPP

constexpr int start_dist = 0;
constexpr int end_dist   = 1301;

constexpr int start_vel    = 0;
constexpr int end_velocity = 101;

constexpr int start_acc = -10;
constexpr int end_acc   = 11;

constexpr int start_ang = -90;
constexpr int end_ang   = 91;

#endif
